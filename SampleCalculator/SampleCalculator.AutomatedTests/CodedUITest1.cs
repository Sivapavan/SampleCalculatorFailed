﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace SampleCalculator.AutomatedTests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest1
    {
        public CodedUITest1()
        {
        }

        [TestMethod]
        public void AdditionAutomated()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //this.UIMap.Addition();
            //this.UIMap.SubtractionNew();

            //this.UIMap.AdditionTwo();
            //this.UIMap.Subtraction();
            //this.UIMap.SubtractionTwo();
            //this.UIMap.AddNew();
            this.UIMap.AddTwoNumbers();
            //this.UIMap.DivisionTwoNumbers();

            //this.UIMap.MultiplyTwoNumbers();

            //this.UIMap.SubtwoNumbers();


            // this.UIMap.AdditionNew();

        }

        [TestMethod]
        public void SubtractionAutomated()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //this.UIMap.Addition();
            //this.UIMap.SubtractionNew();

            //this.UIMap.AdditionTwo();
            //this.UIMap.Subtraction();
            //this.UIMap.SubtractionTwo();
            //this.UIMap.AddNew();
            

            this.UIMap.SubtwoNumbers();


            // this.UIMap.AdditionNew();

        }

        [TestMethod]
        public void MultiplyAutomated()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //this.UIMap.Addition();
            //this.UIMap.SubtractionNew();

            //this.UIMap.AdditionTwo();
            //this.UIMap.Subtraction();
            //this.UIMap.SubtractionTwo();
            //this.UIMap.AddNew();
            //this.UIMap.AddTwoNumbers();
            //this.UIMap.DivisionTwoNumbers();

            this.UIMap.MultiplyTwoNumbers();

            //this.UIMap.SubtwoNumbers();


            // this.UIMap.AdditionNew();

        }

        [TestMethod]
        public void DivisionAutomated()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //this.UIMap.Addition();
            //this.UIMap.SubtractionNew();

            //this.UIMap.AdditionTwo();
            //this.UIMap.Subtraction();
            //this.UIMap.SubtractionTwo();
            //this.UIMap.AddNew();
           // this.UIMap.AddTwoNumbers();
            this.UIMap.DivisionTwoNumbers();

            //this.UIMap.MultiplyTwoNumbers();

            //this.UIMap.SubtwoNumbers();


            // this.UIMap.AdditionNew();

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if (this.map == null)
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
